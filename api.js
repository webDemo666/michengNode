var express = require('express');
var router = express.Router();
var {
    conn
} = require("./utils/db");
var {
    setError,
    aesEncrypt,
    keys,
    aesDecrypt
} = require("./utils");
var {
    User,
    Fuzhuang,
    Xiangqing,
    Meizhuang,
    Peishi,
    Shenghuo,
    Shoushi,
    Wanbiao,
    Splist,
    Car,
    Dizhi,
    Shoucang,
    Pingpaifenlei,
    Ppzimu
} = require("./utils/schema");
var { waterfall } = require("async");

router.get("/", (req, res) => {
    // res.send("这是接口地址 api ~")
    conn((err, db) => {
        setError(err, res, db);
        res.json({
            msg: '连接成功',
            code: 0
        })
        db.disconnect();
    })

})

// 以下所有接口必须要 token 才可以进入

router.get("/ceshi", (req, res) => {
    conn((err, db) => {
        setError(err, res, db);
        res.json({
            msg: '连接成功',
            code: 0
        })
        db.disconnect();
    })

})

// -------------------------------------------------- 注册接口 -------------------------------------------------- //
router.post('/register', (req, res) => {
    var body = req.body;
    var username = body.username;
    conn((err, db) => {
        setError(err, res, db);
        waterfall([
            (cb) => {
                User.findOne({ username }, (err, result) => {
                    cb(err, result);
                })
            },
            (args, cb) => {
                if (args) {
                    cb(err, { msg: "该账号已存在", code: 401, type: 0 })
                } else {
                    User.create(body, (err, result) => {
                        cb(err, { msg: "注册成功", code: 0, type: 1 })
                    })
                }
            }
        ], (err, result) => {
            setError(err, res, db);
            res.json(result);
            db.disconnect();
        })
    })
})



// -------------------------------------------------- 登录接口 -------------------------------------------------- //
router.post("/login", (req, res) => {
    var body = req.body;
    var username = body.username;
    var password = body.password;
    conn((err, db) => {
        setError(err, res, db);
        User.findOne({
            username,
            password
        }, (err, result) => {
            setError(err, res, db);
            console.log(result, 'result')
            if (result) {
                console.log(body.username, 'body.username')
                var token = aesEncrypt(body.username, keys);
                console.log(token);
                req.session.token = token;
                console.log(req.session.token, '后端存储的token');

                res.json({
                    msg: '登录成功',
                    code: 0,
                    type: 1,
                    token,
                })
            } else {
                res.json({
                    msg: '登录失败,请重新登录...',
                    code: 400,
                    type: 0,
                })
            }
            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取服装json -------------------------------------------------- //
router.post("/fuzhuang", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Fuzhuang.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取美妆json -------------------------------------------------- //
router.post("/meizhuang", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Meizhuang.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取配饰 -------------------------------------------------- //
router.post("/peishi", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Peishi.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取生活 -------------------------------------------------- //
router.post("/shenghuo", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Shenghuo.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取首饰 -------------------------------------------------- //
router.post("/shoushi", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Shoushi.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取腕表 -------------------------------------------------- //
router.post("/wanbiao", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Wanbiao.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 搜索商品 -------------------------------------------------- //
router.post("/splist", (req, res) => {
    var body = req.body;
    var goodsname = body.goodsname;
    conn((err, db) => {
        setError(err, res, db)
        Splist.find({
            product_name: new RegExp(goodsname)
        }, {}).exec((err, result) => {
            if (result.length !== 0) {
                setError(err, res, db);
                res.json({
                    msg: '搜索成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '暂无该商品',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 商品总数 -------------------------------------------------- //
router.post("/splistzongshu", (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Splist.find({}, {}).count((err, result) => {
            if (result.length !== 0) {
                setError(err, res, db);
                res.json({
                    msg: '获取总数成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '无商品',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 查询所有收藏商品 -------------------------------------------------- //
router.post("/shoucanglist", (req, res) => {
    var body = req.body;
    var gid = body.gid;
    conn((err, db) => {
        setError(err, res, db)
        Splist.find({
            gid
        }, {}).exec((err, result) => {
            if (result.length !== 0) {
                setError(err, res, db);
                res.json({
                    msg: '搜索成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '赞无收藏',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})



// -------------------------------------------------- 获取详情页 -------------------------------------------------- //
router.post('/xiangqingye', (req, res) => {
    var body = req.body;
    var gid = body.gid;
    console.log(gid, '-----------------gid----------------');
    conn((err, db) => {
        setError(err, res, db);
        Xiangqing.findOne({
            gid
        }, (err, result) => {
            setError(err, res, db);
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取购物车数据 -------------------------------------------------- //
router.post('/getShopCar', (req, res) => {
    var body = req.body;
    var name = body.name;
    conn((err, db) => {
        setError(err, res, db);
        Car.find({ name }).sort({ _id: -1 }).exec((err, result) => {
            setError(err, res, db);
            res.json({
                msg: "获取购物车信息成功",
                code: 0,
                result
            })
            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取购物车总数 -------------------------------------------------- //
router.post('/getShopCarCount', (req, res) => {
    var body = req.body;
    var name = body.name;
    conn((err, db) => {
        setError(err, res, db);
        Car.find({ name }).count((err, result) => {
            setError(err, res, db);
            res.json({
                msg: "获取购物车总数成功",
                code: 0,
                result
            })
            db.disconnect();
        })
    })
})

// -------------------------------------------------- 加入购物车 -------------------------------------------------- //
router.post("/addToCar", (req, res) => {
    var body = req.body;
    console.log(body, 'body')
    var num = body.num * 1;
    var gid = body.gid;
    var name = body.name;

    // 新增 购物车 
    // 1. 判断之前是否存在  
    // 如果存在 直接修改 数量
    // 不存在 直接新增  

    conn((err, db) => {
        setError(err, res, db);
        waterfall([
            (cb) => {
                Car.findOne({ name, gid }, (err, result) => {
                    cb(err, result);
                })
            },
            (args, cb) => {
                if (args) {
                    // 修改数量
                    Car.updateOne({
                        name,
                        gid
                    }, {
                        $set: {
                            num
                        }
                    }, (err, result) => {
                        cb(err, { msg: "增加数量成功", code: 0, result, type: 0 })
                    })
                } else {
                    // 直接插入 
                    // body.time = new Date();
                    // body.username = username;
                    Car.create(body, (err, result) => {
                        cb(err, { msg: "加入购物车成功", code: 0, result, type: 1 })
                    })
                }
            }
        ], (err, result) => {
            setError(err, res, db);
            res.json(result);
            db.disconnect();
        })
    })
})

// -------------------------------------------------- 新增地址 -------------------------------------------------- //
router.post('/xinzengdizhi', (req, res) => {
    var body = req.body;
    conn((err, db) => {
        setError(err, res, db);
        Dizhi.create(body, (err, result) => {
            res.json({
                msg: "地址新增成功",
                code: 0,
            })
            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取所有地址 -------------------------------------------------- //
router.post('/dizhi', (req, res) => {
    var body = req.body;
    var username = body.username;
    conn((err, db) => {
        setError(err, res, db);
        Dizhi.find({ username }).sort({ _id: -1 }).exec((err, result) => {
            setError(err, res, db);
            res.json({
                msg: "获取地址成功",
                code: 0,
                result
            })
            db.disconnect();
        })
    })
})

// -------------------------------------------------- 获取收藏商品 判断当前的商品有没有被收藏 如果有就点亮 没有就不点亮 -------------------------------------------------- //
router.post('/shoucang', (req, res) => {
    var body = req.body;
    var username = body.username;
    conn((err, db) => {
        setError(err, res, db);
        Shoucang.find({ username }).exec((err, result) => {
            setError(err, res, db);
            res.json({
                msg: "获取收藏成功",
                code: 0,
                result
            })
            db.disconnect();
        })
    })
})


// // -------------------------------------------------- 新增收藏 -------------------------------------------------- //
// router.post('/shoucangxinzeng', (req, res) => {
//     var body = req.body;
//     var username = body.username;
//     conn((err,db)=>{
//         setError(err,res,db);
//         Shoucang.find({username}).exec((err,result)=>{
//             setError(err,res,db);
//             res.json({
//                 msg: "获取收藏成功",
//                 code: 0,
//                 result
//             })
//             db.disconnect();
//         })
//     })
// })


// -------------------------------------------------- 新增收藏 -------------------------------------------------- //
router.post('/shoucangxinzeng', (req, res) => {
    var body = req.body;
    var username = body.username;
    var gid = body.gid
    conn((err, db) => {
        setError(err, res, db);
        waterfall([
            (cb) => {
                Shoucang.findOne({ username, gid }, (err, result) => {
                    cb(err, result);
                })
            },
            (args, cb) => {
                if (args) {
                    Shoucang.remove({ gid }, (err, result) => {
                        cb(err, { msg: "已取消收藏", code: 0, type: 2 })
                    })

                } else {
                    Shoucang.create(body, (err, result) => {
                        cb(err, { msg: "收藏成功", code: 0, type: 1 })
                    })
                }
            }
        ], (err, result) => {
            setError(err, res, db);
            res.json(result);
            db.disconnect();
        })
    })
})





// -------------------------------------------------- 删除商品 -------------------------------------------------- //
router.post("/delSelect", (req, res) => {

    var body = req.body;
    var gid = body.gid;
    var name = body.username;
    console.log(gid, name)
    conn((err, db) => {
        setError(err, res, db);
        Car.deleteMany({
            name,
            gid
        }, (err, result) => {
            setError(err, res, db);
            res.json({
                msg: "删除成功",
                code: 0,
                result
            })
            db.disconnect();
        })
    })
})

// -------------------------------------------------- 删除多条 -------------------------------------------------- //
router.post("/delSelectqb", (req, res) => {
    var body = req.body;
    var name = body.username;
    const idArr = req.body.idArr; // type: string
    // const newIdArr = idArr.slice(","); //格式化为我们需要的

    console.log(name, idArr)
    conn((err, db) => {
        setError(err, res, db);
        waterfall([
            (cb) => {
                Car.find({ name }, (err, result) => {
                    cb(err, result);
                })
            },
            (args, cb) => {
                if (args) {
                    Car.remove({ gid: {$in: idArr} }, (err, result) => {
                        cb(err, { msg: "删除成功", code: 0, type: 2 })
                    })
                }
            }
        ], (err, result) => {
            setError(err, res, db);
            res.json(result);
            db.disconnect();
        })
    })
})




// -------------------------------------------------- 分页查询 -------------------------------------------------- //

// router.post('/pagelist', function (req, res, next) {
//     var limit = req.body.pageData || 8;
//     var currentPage = req.body.page || 1;

//     if (currentPage < 1) {
//         currentPage = 1;
//     }
//     conn((err, db) => {
//         setError(err, res, db)
//         Splist.find({}).exec(function (err, rs) {
//             if (err) {
//                 res.send(err);
//             } else {
//                 var totalPage = Math.floor(rs.length / limit);
//                 if (rs.length % limit != 0) {
//                     totalPage += 1;
//                 }
    
//                 if (currentPage > totalPage) {
//                     currentPage = totalPage;
//                 }
//                 console.log(currentPage);
//                 var query = Splist.find({});
//                 query.skip((currentPage - 1) * limit);
//                 query.limit(limit);
//                 query.exec(function (err, result) {
//                     jsonArray = {totalCount: rs.length, data: result, code: 0};
//                     res.json(jsonArray);

//                     db.disconnect();
//                 });
//             }
//         });
//     })
// });


// -------------------------------------------------- 搜索分页查询 -------------------------------------------------- //
router.post('/pagelist', function (req, res, next) {
    var limit = req.body.pageData || 8;
    var currentPage = req.body.page || 1;
    var goodsname = req.body.product_name || '';

    var paixu = req.body.paixu || 1

    console.log(paixu,'paixu')
    if (currentPage < 1) {
        currentPage = 1;
    }
    conn((err, db) => {
        setError(err, res, db)
        Splist.find({
            product_name: new RegExp(goodsname)
        }).sort({price: paixu}).exec(function (err, rs) {
            if (err) {
                res.send(err);
            } else {
                var totalPage = Math.floor(rs.length / limit);
                if (rs.length % limit != 0) {
                    totalPage += 1;
                }
    
                if (currentPage > totalPage) {
                    currentPage = totalPage;
                }
                console.log(currentPage);
                var query = Splist.find({product_name: new RegExp(goodsname)});
                query.skip((currentPage - 1) * limit);
                query.limit(limit);
                query.sort({price: paixu});
                query.exec(function (err, result) {
                    jsonArray = {totalCount: rs.length, data: result, code: 0};
                    res.json(jsonArray);

                    db.disconnect();
                });
            }
        });
    })
});




// -------------------------------------------------- 搜索对应的分类商品 -------------------------------------------------- //
router.post("/huoqufenlei", (req, res) => {
    var body = req.body;
    var goodsType = body.goodsType;
    conn((err, db) => {
        setError(err, res, db)
        Splist.find({
            goods_type: new RegExp(goodsType)
        }, {}).exec((err, result) => {
            if (result.length !== 0) {
                setError(err, res, db);
                res.json({
                    msg: '搜索成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '暂无该商品',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取所有品牌 -------------------------------------------------- //
router.post('/pingpaifenlei', (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Pingpaifenlei.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


// -------------------------------------------------- 获取所有品牌字母开头 -------------------------------------------------- //
router.post('/ppzimu', (req, res) => {
    conn((err, db) => {
        setError(err, res, db)
        Ppzimu.find({}, {}).exec((err, result) => {
            if (result) {
                setError(err, res, db);
                res.json({
                    msg: '获取数据成功',
                    code: 0,
                    result
                })

                db.disconnect();
            } else {
                res.json({
                    msg: '获取数据失败',
                    code: 401
                })
            }

            db.disconnect();
        })
    })
})


module.exports = router;