var mongoose = require("mongoose");
var Schema = mongoose.Schema;


var users_schema = new mongoose.Schema({
    username: String,
    password: String
});
var User = mongoose.model("Users", users_schema);

exports.User = User;

var fuzhuang_schema = new mongoose.Schema()
var Fuzhuang = mongoose.model("Fuzhuangs", fuzhuang_schema);
exports.Fuzhuang = Fuzhuang;


var meizhuang_schema = new mongoose.Schema()
var Meizhuang = mongoose.model("Meizhuangs", meizhuang_schema);
exports.Meizhuang = Meizhuang;


var peishi_schema = new mongoose.Schema()
var Peishi = mongoose.model("Peishis", peishi_schema);
exports.Peishi = Peishi;


var shenghuo_schema = new mongoose.Schema()
var Shenghuo = mongoose.model("Shenghuos", shenghuo_schema);
exports.Shenghuo = Shenghuo;


var shoushi_schema = new mongoose.Schema()
var Shoushi = mongoose.model("Shoushis", shoushi_schema);
exports.Shoushi = Shoushi;


var wanbiao_schema = new mongoose.Schema()
var Wanbiao = mongoose.model("Wanbiaos", wanbiao_schema);
exports.Wanbiao = Wanbiao;


var spList_schema = new mongoose.Schema()
var Splist = mongoose.model("Splists", spList_schema);
exports.Splist = Splist;



var xiangqingye_schema = new mongoose.Schema()
var Xiangqing = mongoose.model("Xiangqingyes", xiangqingye_schema);
exports.Xiangqing = Xiangqing;


var pingpaifenlei_schema = new mongoose.Schema()
var Pingpaifenlei = mongoose.model("Pingpais", pingpaifenlei_schema);
exports.Pingpaifenlei = Pingpaifenlei;

var ppzimu_schema = new mongoose.Schema()
var Ppzimu = mongoose.model("Ppzimus", ppzimu_schema);
exports.Ppzimu = Ppzimu;



var shoucang_schema = new mongoose.Schema({
    username: String,
    gid: String
})
var Shoucang = mongoose.model("Shoucangs", shoucang_schema);
exports.Shoucang = Shoucang;



var cars_schema = new mongoose.Schema({
    bt: String,
    cc: String,
    gid: String,
    img: String,
    jg: String,
    num: Number,
    ys: String,
    name: String
});
var Car = mongoose.model("Gouwuches", cars_schema);

exports.Car = Car;



var dizhi_schema = new mongoose.Schema({
    addressDetail: String,
    areaCode: String,
    city: String,
    country: String,
    county: String,
    isDefault: String,
    name: String,
    postalCode: String,
    province: String,
    tel: String,
    username: String
});
var Dizhi = mongoose.model("Dizhis", dizhi_schema);

exports.Dizhi = Dizhi;